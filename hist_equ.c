#include<stdio.h>
#include <math.h>
#include"pgmlib.h"
long int hist_arry[256]; /* 1d array for histogram*/
double gamma_arry[256];
int equ_arry[256];

void calc_histogram(int n)
/* generate the histogram for No.n image */
{
	int i, x, y;

	for (i = 0; i < 256; i++) hist_arry[i] = 0;  // initialize the histogram
	for (y = 0; y < height[n]; y++)         // scan the image and count the number of intensity
		for (x = 0; x < width[n]; x++)
			hist_arry[image[n][x][y]]++;
}

void make_histogram_image(int h, int n, int source)
/* generate a histogram image (256 pixel width, h pixel height)*/
/* Save the histogram image in No.n image*/
{
	calc_histogram(source);
	int i, x, y;
	long int max; /* max count*/
	int takasa;   /* the height of histogram for each intensity*/

				  /* set the size of histogram image */
	width[n] = 256;
	height[n] = h;
	max = 0;
	for (i = 0; i < 256; i++)
		if (hist_arry[i] > max) max = hist_arry[i];
	/* normalize the count between 0 and h */
	for (x = 0; x < width[n]; x++) {
		takasa = (int)(h / (double)max * hist_arry[x]);
		if (takasa > h) takasa = h;
		for (y = 0; y < h; y++)
			if (y < takasa) image[n][x][h - 1 - y] = 0;
			else image[n][x][h - 1 - y] = 255;
	}
}
void reverse_image(int n)
/* reverse the intensity of image No.n */
{
	int x, y;

	for (y = 0; y < height[n]; y++) {
		for (x = 0; x < width[n]; x++) {
			image[n][x][y] = 255 - image[n][x][y];
		}
	}
}
void gamma_trans(double param) {
	for (int i = 0; i < 256; i++)
	{
		gamma_arry[i] = (pow((i / 255.0), 1.0 / param));
	}
}
//変換が終わったものを引数に入れる(round済)
void make_curve(int h, int n, int* arry) {
	int x, y;
	width[n] = height[n] = h;
	
	for (y = 0; y < height[n]; y++) {
		for (x = 0; x < width[n]; x++) {
			image[n][x][y] = 255;
		}
	}
	for (int i = 0; i < width[n]; i++)
	{
		image[n][i][h - 1 - arry[i]] = 0;
	}
}
void make_filtered_image(int h, int n, int * trans_table) {
	int x, y;
	for (y = 0; y < height[n]; y++) {
		for (x = 0; x < width[n]; x++) {
			image[n][x][y] = trans_table[image[n][x][y]];
		}
	}
}
//n = 入力画像(0-4) L = 最大輝度値
void calc_equalization(int n,int L) {
	calc_histogram(n); //Nf(度数)
	int Nf[256];
	double pf[256];
	int i;
	int N = 0;//N=画素数の合計
	for (i = 0; i < 256; i++)
	{
		Nf[i] = hist_arry[i];
		N += hist_arry[i];
	}
	for (i = 0; i < 256; i++)
		pf[i] = (double)Nf[i] / (double)N;
	double Pf[256];
	//後ろの値がないので０だけ
	Pf[0] = pf[0];
	for (i = 1; i < 256; i++)
	{
		Pf[i] = pf[i]+Pf[i-1];
	}
	//round
	for (i = 0; i <256; i++)
	{
		equ_arry[i] = round((L - 1)*Pf[i]);
	}

}

int main(void)
{
	/*
	No 0 ORG
	No 1 fall_org_hist.pgm
	No 2 fall_trans.pgm //curve
	No 3 fall_hist_equ.pgm
	No 4 fall_trans_hist.pgm
	*/
	load_image(0, "fall.pgm");   /* load image into No.0 */
	for (int i = 1; i < 5; i++)
		copy_image(0, i);
	make_histogram_image(256, 1, 0); /* save the histogram to No.1 image*/
	calc_equalization(0, 255);
	make_curve(256, 2,equ_arry);
	make_filtered_image(256, 3,equ_arry);
	make_histogram_image(256, 4, 3);
	for (int i = 1; i < 5; i++)
	{
		save_image(i, "");
	}
	return 0;
}
